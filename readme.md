#Sheltered Installation
## Windows
This software was developed on and designed for Windows.
Other platforms may work, but are untested and may require recompilation.

Install OpenJDK 8 and run the Jarfile
1. Get JRE/JDK 8 update 251 (Includes JavaFX) here
    > https://www.oracle.com/java/technologies/javase-jdk8-downloads.html

2. Run the jarfile using java
    >java -jar /path_to_jar/Sheltered-1.0-SNAPSHOT-jar-with-dependencies.jar
3. If database connection cannot be established, see section below to build a local database
4. Create an account and log in

### If building local database (more reliable)
1. Install MySQL server https://dev.mysql.com/downloads/mysql/
2. Add a root user
3. Right click on `Sheltered-1.0-SNAPSHOT-jar-with-dependencies.jar` 
4. Open with 7zip / WinRAR
5. Navigate to hibernate.cfg.xml
6. Comment out the remote configuration
7. Uncomment the local database configuration
8. Enter your local db username and password in the specified fields
9. Save the file
10. Update the archive
11. Run using java



