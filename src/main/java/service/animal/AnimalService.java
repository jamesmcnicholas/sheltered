package service.animal;

import model.animal.Animal;
import service.DatabaseConnection;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

/**
 *  Database service class for accessing animals
 *  Allows basic CRUD functions, as well as getting all animals
 */
public class AnimalService extends DatabaseConnection {

    private final SessionFactory sessionFactory;

    public AnimalService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void create(Animal animal){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.save(animal);

        session.getTransaction().commit();
        session.close();
    }

    public void update(Animal animal){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.update(animal);

        session.getTransaction().commit();
        session.close();
    }

    public void delete(Animal animal){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(animal);

        session.getTransaction().commit();
        session.close();
    }

    public Animal getAnimal(int id){
        Session session = sessionFactory.openSession();
        Animal animal = session.get(Animal.class, id);
        return animal;
    }

    public List<Animal> getAllAnimals(){
        Session session = sessionFactory.openSession();
        String hql = "FROM Animal";
        Query query = session.createQuery(hql);
        return (List<Animal>) query.list();
    }



    public void exit() {
        sessionFactory.close();
    }
}
