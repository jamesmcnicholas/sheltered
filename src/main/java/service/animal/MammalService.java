package service.animal;

import model.animal.Mammal;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * An extension of the AnimalService class allowing for CRUD operations on Mammals
 */
public class MammalService extends AnimalService {

    private final SessionFactory sessionFactory;

    public MammalService(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    public void create(Mammal animal){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.save(animal);

        session.getTransaction().commit();
        session.close();
    }

    public void update(Mammal animal){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.update(animal);

        session.getTransaction().commit();
        session.close();
    }

    public void delete(Mammal animal){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(animal);
        session.getTransaction().commit();
        session.close();
    }

    public Mammal getMammal(int id){
        Session session = sessionFactory.openSession();
        Mammal mammal = session.get(Mammal.class, id);
        return mammal;
    }
}
