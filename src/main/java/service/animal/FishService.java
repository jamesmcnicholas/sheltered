package service.animal;

import model.animal.Fish;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * An extension of the AnimalService class allowing for CRUD operations on Fish
 */
public class FishService  extends AnimalService{

    private final SessionFactory sessionFactory;

    public FishService(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    public void create(Fish animal){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.save(animal);

        session.getTransaction().commit();
        session.close();
    }

    public void update(Fish animal){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.update(animal);

        session.getTransaction().commit();
        session.close();
    }

    public void delete(Fish animal){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(animal);

        session.getTransaction().commit();
        session.close();
    }

    public Fish getFish(int id){
        Session session = sessionFactory.openSession();
        Fish fish = session.get(Fish.class, id);
        return fish;
    }
}
