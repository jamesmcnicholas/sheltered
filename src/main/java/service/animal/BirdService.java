package service.animal;

import model.animal.Bird;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * An extension of the AnimalService class allowing for CRUD operations on Birds
 */
public class BirdService extends AnimalService {
    private final SessionFactory sessionFactory;

    public BirdService(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    public void create(Bird animal){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.save(animal);

        session.getTransaction().commit();
        session.close();
    }

    public void update(Bird animal){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.update(animal);

        session.getTransaction().commit();
        session.close();
    }

    public void delete(Bird animal){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(animal);

        session.getTransaction().commit();
        session.close();
    }

    public Bird getBird(int id){
        Session session = sessionFactory.openSession();
        Bird bird = session.get(Bird.class, id);
        return bird;
    }

    public void exit() {
        sessionFactory.close();
    }
}
