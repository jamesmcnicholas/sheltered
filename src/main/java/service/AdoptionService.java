package service;

import model.Adoption;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

/**
 *  Database service class for accessing adoptions
 *  Allows basic CRUD functions, as well as getting all adoptions, and those for a specified user
 */
public class AdoptionService extends DatabaseConnection{

    protected SessionFactory sessionFactory;

    public AdoptionService(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public void create(Adoption adoption){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.save(adoption);

        session.getTransaction().commit();
        session.close();
    }

    public void update(Adoption adoption){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.update(adoption);

        session.getTransaction().commit();
        session.close();
    }

    public void delete(Adoption adoption){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(adoption);

        session.getTransaction().commit();
        session.close();
    }

    public Adoption getAdoption(int adoptionID){
        Session session = sessionFactory.openSession();
        Adoption adoption = session.get(Adoption.class, adoptionID);
        return adoption;
    }

    public List<Adoption> getAdoptionsByUser(int userId){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Adoption WHERE userID IS " + userId);
        return (List<Adoption>) query.list();
    }

    public List<Adoption> getAdoptionsByAnimal(int animalID){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Adoption WHERE animalID IS " + animalID);
        return (List<Adoption>) query.list();
    }

    public List<Adoption> getAllAdoptions(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM  Adoption");
        return (List<Adoption>) query.list();
    }

    public void exit() {
        sessionFactory.close();
    }
}