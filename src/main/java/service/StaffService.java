package service;

import model.account.Staff;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


/**
 *  Database service class for accessing Staff
 *  Allows creation of Staff, which store user permissions
 */
public class StaffService extends UserService {

    public StaffService(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public void create(Staff user){
        Session session = super.sessionFactory.openSession();
        session.beginTransaction();

        session.save(user);

        session.getTransaction().commit();
        session.close();
    }

    public Staff getStaff(int userID){
        Session session = super.sessionFactory.openSession();
        session.beginTransaction();

        Staff staff = session.get(Staff.class, userID);

        session.getTransaction().commit();
        session.close();

        return staff;
    }
}
