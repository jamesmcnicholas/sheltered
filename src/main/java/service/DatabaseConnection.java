package service;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

//TODO maybe pass this in instead of just the sessionFactory
public class DatabaseConnection {
    private SessionFactory sessionFactory;

    /**
     *  Basic template Database service class
     *  Contains sessionFactory setup method, loading from hibernate config
     *  Allows basic CRUD functions
     */
    public void setup() {
        // Loads Hibernate Session factory
        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml") // configures settings from hibernate.cfg.xml
                .build();
        try {
            this.sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        } catch (Exception ex) {
            ex.printStackTrace();
            StandardServiceRegistryBuilder.destroy(registry);
        }
    }

    public void exit() {
        sessionFactory.close();
    }

    public void create(Object object) {}

    public SessionFactory getSessionFactory(){
        return this.sessionFactory;
    }

    public void update() { }

    public void delete() { }

}
