package service;

import model.account.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

/**
 *  Database service class for accessing users
 *  Allows CRUD operations on users
 */
public class UserService extends DatabaseConnection{

    protected SessionFactory sessionFactory;

    public UserService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void create(User user){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.save(user);

        session.getTransaction().commit();
        session.close();
    }

    public void update(User user){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.update(user);

        session.getTransaction().commit();
        session.close();
    }

    public void delete(User user){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(user);

        session.getTransaction().commit();
        session.close();
    }

    public User getUser(int userID){
        Session session = sessionFactory.openSession();
        User user = session.get(User.class, userID);
        // TODO. More closes for get methods
        session.close();
        return user;
    }

    public List<User> getAllUsers(){
        Session session = sessionFactory.openSession();
        String hql = "FROM User";
        Query query = session.createQuery(hql);
        return (List<User>) query.list();
    }

    public void exit() {
        sessionFactory.close();
    }
}
