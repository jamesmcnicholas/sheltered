package model;

/**
 * Enum for storing the types of animals.
 * Can be expanded
 */
public enum AnimalGroup {
    BIRD,
    FISH,
    MAMMAL
}

