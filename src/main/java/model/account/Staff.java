package model.account;

import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * Staff is an extension of User, which adds permission fields
 */
@Entity
@Table(name = "staff")
public class Staff extends User {
    private boolean canAddAnimals;
    private boolean canRemoveAnimals;
    private boolean canManageUsers;
    private boolean canApproveAdoptions;

    public Staff(){
        super();
        this.canAddAnimals = false;
        this.canRemoveAnimals = false;
        this.canManageUsers = false;
        this.canApproveAdoptions = false;
    }

    public Staff(String username, String passwordHash, String salt, boolean canAddAnimals, boolean canRemoveAnimals, boolean canManageUsers, boolean canApproveAdoptions) {
        super(username, passwordHash, salt);
        this.canAddAnimals = canAddAnimals;
        this.canRemoveAnimals = canRemoveAnimals;
        this.canManageUsers = canManageUsers;
        this.canApproveAdoptions = canApproveAdoptions;
    }

    public Staff(User user, boolean canAddAnimals, boolean canRemoveAnimals, boolean canManageUsers, boolean canApproveAdoptions){
        super(user.getUserID(), user.getUsername(), user.getPasswordHash(), user.getSalt(), user.isApproved());
        this.canAddAnimals = canAddAnimals;
        this.canRemoveAnimals = canRemoveAnimals;
        this.canManageUsers = canManageUsers;
        this.canApproveAdoptions = canApproveAdoptions;

    }

    public Staff(User user){
        super(user.getUsername(), user.getPasswordHash(), user.getSalt());
        this.canAddAnimals = false;
        this.canRemoveAnimals = false;
        this.canManageUsers = false;
        this.canApproveAdoptions = false;

    }

    public boolean canAddAnimals() {
        return canAddAnimals;
    }

    public boolean canRemoveAnimals() {
        return canRemoveAnimals;
    }

    public boolean canManageUsers() {
        return canManageUsers;
    }

    public void setCanAddAnimals(boolean canAddAnimals) {
        this.canAddAnimals = canAddAnimals;
    }

    public void setCanRemoveAnimals(boolean canRemoveAnimals) {
        this.canRemoveAnimals = canRemoveAnimals;
    }

    public void setCanManageUsers(boolean canManageUsers) {
        this.canManageUsers = canManageUsers;
    }

    public boolean canApproveAdoptions(){ return this.canApproveAdoptions;}

    public void setCanApproveAdoptions(boolean canApproveAdoptions) {
        this.canApproveAdoptions = canApproveAdoptions;
    }
}
