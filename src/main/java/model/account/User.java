package model.account;

import javax.persistence.*;

/**
 * User stores user related infomation, such as username, passwordHash value and preferences such as theme colour
 * User maps to the Users table in the database
 */
@Entity
@Table (name="users")
@Inheritance(strategy = InheritanceType.JOINED)
public class User {
    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int userID;
    private String username;
    private String passwordHash;
    private String salt;
    private boolean approved;
    private String themeColour;
    //TODO. store theme colour

    public User(){}
    // New User from scratch
    public User(String username, String passwordHash, String salt) {
        this.passwordHash = passwordHash;
        this.salt = salt;
        this.username = username;
        this.approved = false;
        this.themeColour = "#d04649";
    }

    public User(int userID, String username, String passwordHash, String salt) {
        this.userID = userID;
        this.passwordHash = passwordHash;
        this.salt = salt;
        this.username = username;
        this.approved = false;
        this.themeColour = "#d04649";
    }
    // New User from existing user
    public User(int userID, String username, String passwordHash, String salt, boolean isApproved) {
        this.userID = userID;
        this.passwordHash = passwordHash;
        this.salt = salt;
        this.username = username;
        this.approved = isApproved;
        this.themeColour = "#d04649";
    }


    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }


    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public boolean isApproved(){ return this.approved; }

    public void setApproved(boolean approved){
        this.approved = approved;
    }

    public String getThemeColour() {
        return themeColour;
    }

    public void setThemeColour(String themeColour) {
        this.themeColour = themeColour;
    }

    @Override
    public String toString() {
        return userID + ": " + username;
    }

}
