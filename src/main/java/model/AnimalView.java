package model;

import javafx.beans.property.SimpleStringProperty;
import model.animal.Animal;

/**
 * A class representing Animal information in a format TableView can use
 * Attributes are mapped to SimpleStringProperties
 */
public class AnimalView {
    private final SimpleStringProperty animalID;
    private final SimpleStringProperty name;
    private final SimpleStringProperty description;
    private final SimpleStringProperty animalGroup;
    private final SimpleStringProperty cost;

    public AnimalView(Animal animal){
        this.animalID = new SimpleStringProperty(Integer.toString(animal.getID()));
        this.description = new SimpleStringProperty(animal.getDescription());
        this.name = new SimpleStringProperty(animal.getName());
        this.animalGroup = new SimpleStringProperty(animal.getAnimalGroup().toString());
        this.cost = new SimpleStringProperty("£" + animal.getCost());
    }

    public String getName() { return name.get(); }

    public String getAnimalGroup() { return animalGroup.get(); }

    public String getDescription(){ return description.get(); }

    public String getCost(){ return cost.get(); }

    public String getAnimalID(){ return animalID.get(); }
}
