package model;

import model.account.User;
import model.animal.Animal;
import org.hibernate.SessionFactory;
import service.UserService;
import service.animal.AnimalService;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Class stores adoption information
 * AdoptionID - composite promary key consisting of animalID and userID
 * Approved - Whether or not the adoption is approved for the user
 */

@Entity
@Table(name="adoptions")
public class Adoption {
    @Id
    private CompoundAdoptionID adoptionId;
    private boolean approved;

    public Adoption(CompoundAdoptionID id) {
        this.adoptionId = id;
        this.approved = false;
    }

    public Adoption() { }

    public CompoundAdoptionID getAdoptionId() {
        return adoptionId;
    }

    public void setAdoptionId(CompoundAdoptionID adoptionId) {
        this.adoptionId = adoptionId;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    // Outputs limited information due to lack of access to service classes
    public String toString() {
        // TODO re-enable this
        return "UserID: " + adoptionId.getUserID() + " requested AnimalID: " + adoptionId.getAnimalID();
    }

    /**
     * AdoptionID is a composite primary key.
     * This is generated from the ID of the user requesting the adoption, and the ID of the animal
     * This is unique and allows linkage to other tables, using each ID as a foreign key
     * @Embeddable allows this to be embedded in the Adoption class as a single attribute
     */
    @Embeddable
public static class CompoundAdoptionID implements Serializable {
    private int userID;
    private int animalID;

    public CompoundAdoptionID() { }

    public CompoundAdoptionID(int userID, int animalID) {
        this.userID = userID;
        this.animalID = animalID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getAnimalID() {
        return animalID;
    }

    public void setAnimalID(int animalID) {
        this.animalID = animalID;
    }

    // Overridden for checking if an adoption exists
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompoundAdoptionID that = (CompoundAdoptionID) o;
        return userID == that.userID &&
                animalID == that.animalID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(userID, animalID);
    }
}

}
