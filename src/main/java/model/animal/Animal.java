package model.animal;

import com.jfoenix.controls.datamodels.treetable.RecursiveTreeObject;
import model.AnimalGroup;

import java.util.Objects;
import javax.persistence.*;

/**
 * The base Animal class
 * This contains the basic attributes that any animal can share
 * Class is mapped to the animals table in the database
 * Animals must at least contain the information displayed in the TableView
 * This includes name, group, description, cost and image path
 * Other attributes are optional
 */

@Entity
@Table(name="animals")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Animal {
    @Id
    @Column(name = "animal_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private AnimalGroup animalGroup;
    private String name;
    private String description;
    private String cost;
    private String image;
    private boolean adopted;

    public Animal(String name, AnimalGroup animalGroup, String description, String cost, String image) {
        this.name = name;
        this.animalGroup = animalGroup;
        this.description = description;
        this.cost = cost;
        this.image = image;
        this.adopted = false;
    }

    public Animal() {

    }

    public Animal(Animal animal) {
        this.name = animal.getName();
        this.animalGroup = animal.getAnimalGroup();
        this.description = animal.getDescription();
        this.cost = animal.getCost();
        this.image = animal.getImage();
    }

    /**
     * Getters and setters for animal attributes
     */

    public int getID(){
        return this.id;
    }

    public void setID(int id){
        this.id = id;
    }

    public AnimalGroup getAnimalGroup() {
        return animalGroup;
    }

    public void setAnimalGroup(AnimalGroup animalGroup) {
        this.animalGroup = animalGroup;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCost(){ return this.cost; }

    public void setCost(String cost){ this.cost = cost; }

    public String getImage() { return this.image; }

    public void setImage(String image){ this.image = image; }

    // If an animal is marked as adopted, it belongs to a user, and will not be shown in the table view
    // Staff with the associated permission can remove an adoption
    public void setAdopted(boolean adopted){
        this.adopted = adopted;
    }

    public boolean isAdopted(){
        return this.adopted;
    }



    /**
     * Override methods
     */
    // Equals and hashcode are overwritten in order to compare animals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Animal animal = (Animal) o;
        return id == animal.id &&
                adopted == animal.adopted &&
                animalGroup == animal.animalGroup &&
                name.equals(animal.name) &&
                description.equals(animal.description) &&
                cost.equals(animal.cost) &&
                image.equals(animal.image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(animalGroup,  name, description, cost, image);
    }

    // toString() is overridden to provide a more readable string output when writinganimal details to file
    @Override
    public String toString(){
        return String.format("Name: %s, Type: %s, Description: %s, Cost: %s",
                name, animalGroup, description, cost);
    }
}
