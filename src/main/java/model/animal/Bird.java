package model.animal;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Bird is an extension of animal, allowing for the storage of species and colour values
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("Bird")
public class Bird extends Animal {
    private String species;
    public String colour;

    public Bird(Animal animal, String species, String colour) {
        this.setName(animal.getName());
        this.setAnimalGroup(animal.getAnimalGroup());
        this.setDescription(animal.getDescription());
        this.setCost(animal.getCost());
        this.setImage(animal.getImage());
        this.species = species;
        this.colour = colour;
    }

    public Bird(){}

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }
}
