package model.animal;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Fish is an extension of Animal, allowing for the storage of the breed value
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("Mammal")
public class Mammal extends Animal {
    private String breed;

    public Mammal() { }

    public Mammal(String breed) {
        this.breed = breed;
    }

    public Mammal(Animal animal, String breed){
        this.setName(animal.getName());
        this.setAnimalGroup(animal.getAnimalGroup());
        this.setDescription(animal.getDescription());
        this.setCost(animal.getCost());
        this.setImage(animal.getImage());
        this.breed = breed;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }
}
