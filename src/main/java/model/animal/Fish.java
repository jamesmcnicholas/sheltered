package model.animal;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 * Fish is an extension of Animal, allowing for the storage of the freshwater value
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorValue("Fish")
public class Fish  extends Animal{

    boolean freshWater;

    public Fish() {}

    public Fish(Animal animal, boolean freshWater){
        this.setName(animal.getName());
        this.setAnimalGroup(animal.getAnimalGroup());
        this.setDescription(animal.getDescription());
        this.setCost(animal.getCost());
        this.setImage(animal.getImage());
        this.freshWater = freshWater;
    }

    public boolean isFreshWater() {
        return freshWater;
    }

    public void setFreshWater(boolean freshWater) {
        this.freshWater = freshWater;
    }
}
