package controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import service.DatabaseConnection;
import org.hibernate.SessionFactory;

import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main is the starting point for the application
 * This creates the initial DB connection and starts the login screen
 * Source code by James McNicholas (j.mcnicholas.18@unimail.winchester.ac.uk)
 */
public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception {
        // Hide hibernate info logs
        Logger.getLogger("org.hibernate").setLevel(Level.WARNING);
        DatabaseConnection databaseConnection = new DatabaseConnection();
        databaseConnection.setup();
        SessionFactory sessionFactory = databaseConnection.getSessionFactory();

        FXMLLoader loader = new FXMLLoader();
        URL url = getClass().getResource("/loginScreen.fxml").toURI().toURL();
        loader.setLocation(url);
        Parent loginScreenParent = loader.load();

        //Sets up the scene with the elements from the FXML file
        Scene loginScreenScene = new Scene(loginScreenParent);
        // Passes the database sessionFactory to the login screen
        LoginScreenController controller = loader.getController();
        controller.setSessionFactory(sessionFactory);

        primaryStage.setScene(loginScreenScene);
        primaryStage.setTitle("Sheltered Login");
        try{
            primaryStage.getIcons().add(new Image("img/sheltered_logo.png"));
        } catch (IllegalArgumentException illegalArgumentException){
            illegalArgumentException.printStackTrace();
        }
        primaryStage.show();

    }

    // Application entry point, launches start()
    public static void main(String[] args) {
        launch(args);
    }
}