package controller;

import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ToggleGroup;
import model.AnimalView;
import model.animal.Animal;
import service.animal.AnimalService;
import org.hibernate.SessionFactory;
import java.util.List;

/**
 *  Class controlling search and filter related functions
 *  FXML elements are defined in SearchPane.fxml
 */
public class SearchPaneController {
    @FXML
    private JFXTextField searchField;
    @FXML
    private JFXRadioButton nameRadioButton;
    @FXML
    private JFXRadioButton animalRadioButton;
    @FXML
    private JFXRadioButton ageRadioButton;
    @FXML
    private CheckBox descriptionCheckBox;
    @FXML
    private CheckBox nameCheckBox;
    @FXML
    private CheckBox animalCheckBox;
    @FXML
    private CheckBox breedCheckBox;
    @FXML
    private CheckBox costCheckBox;
    @FXML
    private CheckBox ageCheckBox;

    private ToggleGroup toggleGroup;
    private MainScreenController mainScreenController;
    private AnimalService animalService;

    public void initialize(){
        // Add toggle buttons to group
        this.toggleGroup = new ToggleGroup();
        nameRadioButton.setToggleGroup(toggleGroup);
        animalRadioButton.setToggleGroup(toggleGroup);
        ageRadioButton.setToggleGroup(toggleGroup);
        nameRadioButton.setSelected(true);

    }

    public void initService(SessionFactory sessionFactory){
        this.animalService = new AnimalService(sessionFactory);
    }

    void setMainScreenController(MainScreenController mainScreenController){
        this.mainScreenController = mainScreenController;
    }

    //Runs whenever a user enters a key into the search box
    @FXML
    private void search() {
        String query = this.searchField.getText();
        JFXRadioButton selectedSearchToggle = (JFXRadioButton) toggleGroup.getSelectedToggle();
        String criteria = selectedSearchToggle.getText();

        // Grabs all animals, then removes them from the list if they are adopted or do not match criteria
        List<Animal> results = animalService.getAllAnimals();
        switch (criteria) {
            case "Name":
                results.removeIf(animal -> !(animal.getName().toUpperCase().contains(query.toUpperCase())));
                results.removeIf(Animal::isAdopted);
                break;
            case "Description":
                results.removeIf(animal -> !(animal.getDescription().toUpperCase().contains(query.toUpperCase())));
                results.removeIf(Animal::isAdopted);
                break;
            case "Animal":
                results.removeIf(animal -> !(animal.getAnimalGroup().toString().contains(query.toUpperCase())));
                results.removeIf(Animal::isAdopted);
                break;
            default:
                break;
        }

        // Set up a new list and add the results
        ObservableList<AnimalView> animalViews = FXCollections.observableArrayList();
        results.forEach(animal -> animalViews.add(new AnimalView(animal)));
        mainScreenController.updateAnimalTable(animalViews);
    }

    @FXML
    private void cancelSearch() {
        this.searchField.setText("");
        mainScreenController.populateTable();
    }

    @FXML
    void animalCheckBoxToggle() {
        mainScreenController.setAnimalVisible(animalCheckBox.isSelected());
    }

    @FXML
    void nameCheckBoxToggle() {
        mainScreenController.setNameVisible(nameCheckBox.isSelected());
    }

    @FXML
    void costCheckBoxToggle() {
        mainScreenController.setCostVisible(costCheckBox.isSelected());
    }

    @FXML
    void ageCheckBoxToggle() {
        mainScreenController.setAgeVisible(ageCheckBox.isSelected());
    }

    @FXML
    void descriptionCheckBoxToggle() {
        mainScreenController.setDescriptionVisible(descriptionCheckBox.isSelected());
    }
}
