package controller;

import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXListView;
import com.jfoenix.controls.JFXToggleButton;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import model.account.Staff;
import model.account.User;
import service.StaffService;
import service.UserService;
import org.hibernate.SessionFactory;
import java.util.ArrayList;

/**
 * Class controlling user management
 * FXML entities are defined in ManagePane.fxml
 */
public class ManagePaneController {

    @FXML
    private JFXListView<User> userListView;
    @FXML
    private Text selectedUserText;
    @FXML
    private JFXCheckBox accountApprovedCheckBox;
    @FXML
    private JFXToggleButton staffToggleButton;
    @FXML
    private JFXCheckBox canAddAnimalsCheckBox;
    @FXML
    private JFXCheckBox canRemoveAnimalsCheckBox;
    @FXML
    private JFXCheckBox canManageUsersCheckBox;
    @FXML
    private JFXCheckBox canApproveAdoptionsCheckBox;

    private MainScreenController mainScreenController;
    private UserService userService;
    private StaffService staffService;
    private User selectedUser;
    private ArrayList<JFXCheckBox> permissionCheckboxes = new ArrayList<>();
    private ObservableList<User> users = FXCollections.observableArrayList();

    public void initialize(){
        permissionCheckboxes.add(canAddAnimalsCheckBox);
        permissionCheckboxes.add(canRemoveAnimalsCheckBox);
        permissionCheckboxes.add(canManageUsersCheckBox);
        permissionCheckboxes.add(canApproveAdoptionsCheckBox);

        // Lambda function listens to the listview for changes in selected user, then updates the permission checkboxes
        userListView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newUser) -> updatePermissionsView(newUser)
        );
    }

    public void initService(SessionFactory sessionFactory){
        this.staffService = new StaffService(sessionFactory);
        this.userService = new UserService(sessionFactory);

        users.addAll(userService.getAllUsers());
        userListView.setItems(users);
    }

    void setMainScreenController(MainScreenController mainScreenController){
        this.mainScreenController = mainScreenController;
    }

    @FXML
    public void accountApprovedCheckboxToggle() {
        selectedUser.setApproved(accountApprovedCheckBox.isSelected());
        userService.update(selectedUser);
    }

    @FXML
    public void canAddAnimalsCheckboxToggle() {
        Staff staffUser = staffService.getStaff(selectedUser.getUserID());
        staffUser.setCanAddAnimals(canAddAnimalsCheckBox.isSelected());
        staffService.update(staffUser);
    }

    @FXML
    public void canRemoveAnimalsCheckboxToggle() {
        Staff staffUser = staffService.getStaff(selectedUser.getUserID());
        staffUser.setCanRemoveAnimals(canRemoveAnimalsCheckBox.isSelected());
        staffService.update(staffUser);
    }

    @FXML
    public void canManageUsersCheckboxToggle() {
        Staff staffUser = staffService.getStaff(selectedUser.getUserID());
        staffUser.setCanManageUsers(canManageUsersCheckBox.isSelected());
        staffService.update(staffUser);
    }

    @FXML
    public void canApproveAdoptionsCheckboxToggle() {
        Staff staffUser = staffService.getStaff(selectedUser.getUserID());
        staffUser.setCanApproveAdoptions(canApproveAdoptionsCheckBox.isSelected());
        staffService.update(staffUser);
    }
    // Toggles whether or not a user is a staff member
    @FXML
    public void toggleStaff() {
        if (selectedUser != null) {
            selectedUser = userService.getUser(selectedUser.getUserID());
            if (staffToggleButton.isSelected()) {
                Staff staffUser = new Staff(selectedUser, false, false, false, false);
                userService.delete(selectedUser);
                this.selectedUser = staffUser;
                this.staffService.create(staffUser);
                permissionCheckboxes.forEach(checkbox -> checkbox.setDisable(false));
            } else {
                User newUser = new User(
                        selectedUser.getUserID(),
                        selectedUser.getUsername(),
                        selectedUser.getPasswordHash(),
                        selectedUser.getSalt(),
                        selectedUser.isApproved()
                );
                userService.delete(selectedUser);
                userService.create(newUser);
                this.selectedUser = newUser;
                permissionCheckboxes.forEach(checkbox -> checkbox.setDisable(true));
            }
        }
    }

    // Updates permissions elements
    private void updatePermissionsView(User user) {
        accountApprovedCheckBox.setSelected(user.isApproved());
        this.selectedUser = user;
        this.selectedUserText.setText("Selected User: " + selectedUser.getUsername());
        // If the user is staff, enable their checkboxes
        if (staffService.getStaff(user.getUserID()) != null) {
            permissionCheckboxes.forEach(checkbox -> checkbox.setDisable(false));
            // ensure they are shown as staff
            staffToggleButton.setSelected(true);
            Staff staff = staffService.getStaff(user.getUserID());
            canAddAnimalsCheckBox.setSelected(staff.canAddAnimals());
            canRemoveAnimalsCheckBox.setSelected(staff.canRemoveAnimals());
            canManageUsersCheckBox.setSelected(staff.canManageUsers());
            canApproveAdoptionsCheckBox.setSelected(staff.canApproveAdoptions());
        } else {
            staffToggleButton.setSelected(false);
            permissionCheckboxes.forEach(checkbox -> checkbox.setDisable(true));
        }
    }
}
