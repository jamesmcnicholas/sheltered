package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import model.Adoption;
import model.AnimalView;
import model.account.Staff;
import model.animal.Animal;
import model.animal.Bird;
import model.animal.Fish;
import model.animal.Mammal;
import service.AdoptionService;
import service.animal.AnimalService;
import service.StaffService;
import org.hibernate.SessionFactory;
import service.animal.BirdService;
import service.animal.FishService;
import service.animal.MammalService;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;


/**
 * This class controls the animal view pane
 * This is where animals are shown , removed and requested for adoption.
 * FXML entities are defined in AnimalViewPane.fxml
 */
public class AnimalViewPaneController {
    @FXML
    private ImageView animalViewImage;
    @FXML
    private Text animalViewNameText;
    @FXML
    private Text animalViewAgeText;
    @FXML
    private Text animalViewCostText;
    @FXML
    private JFXTextArea animalViewDescriptionText;
    @FXML
    private JFXButton removeAnimalButton;
    @FXML
    private JFXButton exportToFileButton;
    @FXML
    private Text extraText1;
    @FXML
    private Text extraText2;

    private StaffService staffService;
    private AnimalService animalService;
    private AdoptionService adoptionService;
    private MammalService mammalService;
    private FishService fishService;
    private BirdService birdService;
    private AnimalView selectedRow;
    private MainScreenController mainScreenController;
    private AdoptionPaneController adoptionPaneController;

    public void initService(SessionFactory sessionFactory){
        this.staffService = new StaffService(sessionFactory);
        this.animalService = new AnimalService(sessionFactory);
        this.adoptionService = new AdoptionService(sessionFactory);
        this.mammalService = new MammalService(sessionFactory);
        this.birdService = new BirdService(sessionFactory);
        this.fishService = new FishService(sessionFactory);
    }


    public void setSelectedRow(AnimalView animalView){
        this.selectedRow = animalView;
    }

    public void setMainScreenController(MainScreenController mainScreenController) {
        this.mainScreenController = mainScreenController;
        Staff staffUser = staffService.getStaff(mainScreenController.getLoggedInUser().getUserID());
        if (staffUser != null) {
            this.removeAnimalButton.setDisable(!staffUser.canRemoveAnimals());
        }
    }

    public void setAdoptionPaneController(AdoptionPaneController adoptionPaneController){
        this.adoptionPaneController = adoptionPaneController;
    }

    @FXML
    public void removeAnimal() {
        Staff staffUser = staffService.getStaff(mainScreenController.getLoggedInUser().getUserID());
        if (staffUser != null) {
            if (staffUser.canRemoveAnimals()) {
                // Remove any adoption requests relating to this animal before deleting
                List<Adoption> adoptionList = adoptionService.getAdoptionsByAnimal(Integer.parseInt(this.selectedRow.getAnimalID()));
                adoptionList.forEach(adoption -> adoptionService.delete(adoption));
                // Delete animal
                Animal animal = animalService.getAnimal(Integer.parseInt(this.selectedRow.getAnimalID()));
                animalService.delete(animal);
                mainScreenController.populateTable();
            }
        }
    }

    @FXML
    private void requestAdoption() {
        if (this.selectedRow != null) {
            Animal animal = animalService.getAnimal(Integer.parseInt(this.selectedRow.getAnimalID()));
            Adoption.CompoundAdoptionID id = new Adoption.CompoundAdoptionID(mainScreenController.getLoggedInUser().getUserID(), animal.getID());
            Adoption adoption = new Adoption(id);
            this.adoptionService.create(adoption);
        }
        adoptionPaneController.updateAdoptions();
    }

    // Loads an animal into view based on an animalView object
    public void loadAnimalView(AnimalView animalView) {
        Animal animal = animalService.getAnimal(Integer.parseInt(animalView.getAnimalID()));
        this.animalViewNameText.setText("Name: " + animalView.getName());
        this.animalViewAgeText.setText("Animal Type: " + animalView.getAnimalGroup());
        this.animalViewCostText.setText("Cost: " + animalView.getCost());
        this.animalViewDescriptionText.setText("Description: " + animalView.getDescription());
        // Load image file
        try {
            Image newImage = new Image("file:///" + animal.getImage());
            this.animalViewImage.setImage(newImage);
        } catch (Exception e) {
            e.printStackTrace();
            mainScreenController.generateError(e.getMessage(), Arrays.toString(e.getStackTrace()));
        }
        // Checks animal type and loads extra, specific information
        switch(animal.getAnimalGroup()){
            case MAMMAL:
                Mammal mammal = mammalService.getMammal(animal.getID());
                extraText1.setText("Breed: " + mammal.getBreed());
                extraText2.setText("");
                break;
            case FISH:
                Fish fish = fishService.getFish(animal.getID());
                if (fish.isFreshWater()){
                    extraText1.setText("Type: Freshwater fish");
                } else {
                    extraText1.setText("Type: Saltwater fish");
                }
                extraText2.setText("");
                break;
            case BIRD:
                Bird bird = birdService.getBird(animal.getID());
                extraText1.setText("Species: " + bird.getSpecies());
                extraText2.setText("Colour: " + bird.getColour());
                break;
        }
    }

    @FXML
    private void exportAnimalToFile(){
        if(selectedRow != null) {

            try {
                DirectoryChooser directoryChooser = new DirectoryChooser();
                directoryChooser.setTitle("Open Resource File");
                File selectedFile = directoryChooser.showDialog(new Stage());
                Writer writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream( selectedFile.getAbsolutePath() + "/" + selectedRow.getName() + ".txt"), StandardCharsets.UTF_8)
                );
                // Animal's toString method handles formatting
                Animal animal = animalService.getAnimal(Integer.parseInt(selectedRow.getAnimalID()));
                writer.write(animal.toString());
                writer.close();

            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }
}
