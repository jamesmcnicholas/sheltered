package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXTextField;
import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.AnimalGroup;
import model.animal.Animal;
import model.animal.Bird;
import model.animal.Fish;
import model.animal.Mammal;
import service.animal.BirdService;
import service.animal.FishService;
import service.animal.MammalService;
import org.hibernate.SessionFactory;

import java.io.File;

/**
 * This class controls functions for adding new animals
 * FXML elements are defined in AddAnimalPane.FXML
 */
public class AddAnimalPaneController {
    @FXML
    private JFXTextField nameField;
    @FXML
    private JFXTextField descriptionField;
    @FXML
    private JFXTextField costField;
    @FXML
    private JFXTextField imageField;
    @FXML
    private JFXComboBox<AnimalGroup> animalGroupComboBox;
    @FXML
    private JFXButton addAnimalButton;
    @FXML
    private JFXTextField colourField;
    @FXML
    private JFXTextField speciesField;
    @FXML
    private VBox birdBox;
    @FXML
    private JFXTextField breedField;
    @FXML
    private VBox mammalBox;
    @FXML
    private JFXComboBox<String> freshWaterComboBox;
    @FXML
    private VBox fishBox;

    private MainScreenController mainScreenController;
    private File selectedImageFile;
    private BirdService birdService;
    private MammalService mammalService;
    private FishService fishService;


    public void initialize() {
        // Populate animal type combobox
        animalGroupComboBox.getItems().addAll(AnimalGroup.values());
        animalGroupComboBox.setValue(AnimalGroup.MAMMAL);
        updateAnimalGroup();

        freshWaterComboBox.getItems().addAll("Freshwater", "Saltwater");
        freshWaterComboBox.setValue("Freshwater");

    }
    // Sets up services
    public void initService(SessionFactory sessionFactory) {
        this.birdService = new BirdService(sessionFactory);
        this.mammalService = new MammalService(sessionFactory);
        this.fishService = new FishService(sessionFactory);
    }

    void setMainScreenController(MainScreenController mainScreenController) {
        this.mainScreenController = mainScreenController;
    }

    @FXML
    void chooseImageButtonPushed() {
        //TODO. Change to Copy image to a new folder
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif")
        );
        File selectedFile = fileChooser.showOpenDialog(new Stage());
        imageField.setText(selectedFile.getName());
        this.selectedImageFile = selectedFile;
    }

    // Changes visible fields based on selected animal type
    @FXML
    void updateAnimalGroup() {
        birdBox.setVisible(false);
        mammalBox.setVisible(false);
        fishBox.setVisible(false);

        switch (animalGroupComboBox.getValue()){
            case BIRD:
                birdBox.setVisible(true);
                break;
            case MAMMAL:
                mammalBox.setVisible(true);
                break;
            case FISH:
                fishBox.setVisible(true);
                break;
        }
    }

    // Collects information from input fields and adds animal
    @FXML
    void addAnimalButtonPushed() {
        try{
            Float.parseFloat(costField.getText());
        } catch (NumberFormatException numberFormatException){
            mainScreenController.generateError("Invalid option", "Please enter a valid number for cost (e.g. 24.40)");
        }

        Animal newAnimal = new Animal(
            nameField.getText(),
            animalGroupComboBox.getValue(),
            descriptionField.getText(),
            costField.getText(),
            selectedImageFile.getAbsolutePath()
        );

        switch (animalGroupComboBox.getValue()) {
            case MAMMAL:
                Mammal mammal = new Mammal(newAnimal, breedField.getText());
                mammalService.create(mammal);
                break;
            case BIRD:
                Bird bird = new Bird(newAnimal, speciesField.getText(), colourField.getText());
                birdService.create(bird);
                break;
            case FISH:
                boolean freshWater = freshWaterComboBox.getValue().equals("Freshwater");
                Fish fish = new Fish(newAnimal, freshWater);
                fishService.create(fish);
                break;
        }
        mainScreenController.populateTable();
    }
}
