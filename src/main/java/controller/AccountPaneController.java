package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXColorPicker;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.paint.Color;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import model.account.User;
import model.animal.Animal;
import service.AdoptionService;
import service.UserService;
import service.animal.AnimalService;
import org.hibernate.SessionFactory;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * This class controls functions relating to the account pane
 * FXML elements are defined in AccountPane.fxml
 */
public class AccountPaneController {

    @FXML
    private JFXColorPicker themePicker;
    @FXML
    private JFXButton exportToFileButton;

    private MainScreenController mainScreenController;
    private AnimalService animalService;
    private UserService userService;
    private AdoptionService adoptionService;
    private SessionFactory sessionFactory;

    // Sets up services and initialises colour picker
    public void initService(SessionFactory sessionFactory) {
        this.animalService = new AnimalService(sessionFactory);
        this.userService = new UserService(sessionFactory);
        this.adoptionService = new AdoptionService(sessionFactory);
        this.sessionFactory = sessionFactory;
        themePicker.setValue(Color.web(mainScreenController.getLoggedInUser().getThemeColour()));
        changeThemeColour();
    }

    void setMainScreenController(MainScreenController mainScreenController){
        this.mainScreenController = mainScreenController;
    }

    @FXML
    public void logOut() {
        // Same process as loading the main screen, but in reverse
        try {
            //Creates a new FXMLLoader object and loads in the main controller
            FXMLLoader loader = new FXMLLoader();
            URL url = getClass().getResource("/loginScreen.fxml").toURI().toURL();
            loader.setLocation(url);
            Parent loginScreenParent = loader.load();

            LoginScreenController loginScreenController = loader.getController();
            loginScreenController.setSessionFactory(sessionFactory);
            //Sets up the scene with the elements from the FXML file
            Scene loginScreenScene = new Scene(loginScreenParent);

            // Assigns details to the new scene then shows the window
            // Grabs source from an arbitrary element
            Stage window = (Stage) exportToFileButton.getScene().getWindow();
            window.setScene(loginScreenScene);
            window.setTitle("Sheltered");
            window.setFullScreen(false);
            window.show();

        } catch (IOException | URISyntaxException e) {
            System.out.println("Error: " + e.getMessage() + e.getCause());
            e.printStackTrace();
            generateError(e.getMessage(), Arrays.toString(e.getStackTrace()));
        }
    }

    @FXML
    private void quit() {
        System.exit(0);
    }

    @FXML
    private void changeThemeColour() {
        // Gets colour from the picker and converts it to hex
        Color colour = this.themePicker.getValue();
        String hexValue = String.format("#%02X%02X%02X",
                (int) (colour.getRed() * 255),
                (int) (colour.getGreen() * 255),
                (int) (colour.getBlue() * 255));
        mainScreenController.setThemeColour(String.format("-theme-colour: %s;", hexValue));
        // Updates the database with user's new colour
        User loggedInUser = mainScreenController.getLoggedInUser();
        loggedInUser.setThemeColour(hexValue);
        userService.update(loggedInUser);
    }


    @FXML
    private void exportToFile() {
        try {
            // Opens directory chooser dialog
            DirectoryChooser directoryChooser = new DirectoryChooser();
            directoryChooser.setTitle("Open Resource File");
            File selectedFile = directoryChooser.showDialog(new Stage());
            Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(
                            selectedFile.getAbsolutePath() + "/Animal List.txt"), StandardCharsets.UTF_8)
            );

            // Tally up total cost
            float totalCost = 0;
            for (Animal animal : animalService.getAllAnimals()) {
                writer.write(animal.toString() + "\n");
                totalCost += Float.parseFloat(animal.getCost());
            }
            writer.write("Total cost: " + totalCost);
            writer.close();

        } catch (IOException ioe){
            // If location not found
            generateError("File error", ioe.getMessage());
        }
    }

    @FXML
    private void deleteAccount(){
        //Remove any associated adoptions
        User user = mainScreenController.getLoggedInUser();
        adoptionService.getAdoptionsByUser(user.getUserID()).forEach(adoption -> adoptionService.delete(adoption));
        // Delete the user (also removes staff entries)
        userService.delete(user);
        logOut();
    }

    // Helper method for generating error popups
    private void generateError(String message, String stackTrace) {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Error has occurred");
            alert.setHeaderText(message);
            alert.setContentText(stackTrace);
        alert.show();
    }

}
