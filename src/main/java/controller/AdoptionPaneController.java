package controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXListView;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.text.Text;
import model.Adoption;
import model.AnimalView;
import model.account.Staff;
import model.account.User;
import model.animal.Animal;
import service.AdoptionService;
import service.UserService;
import service.animal.AnimalService;
import service.StaffService;
import org.hibernate.SessionFactory;
import java.util.List;

/**
 * This class controls adoption related functions
 * FXML entities are defined in AdoptionPane.fxml
 */
public class AdoptionPaneController {
    @FXML
    private Text selectedAdoptionUserText;
    @FXML
    private JFXListView<Adoption> adoptionListView;
    @FXML
    private JFXCheckBox adoptionApprovedCheckBox;
    @FXML
    private JFXButton cancelAdoptionButton;

    private Adoption selectedAdoption;
    private AnimalService animalService;
    private AdoptionService adoptionService;
    private UserService userService;
    private StaffService staffService;
    private MainScreenController mainScreenController;
    private AnimalViewPaneController animalViewPaneController;
    private ObservableList<Adoption> adoptions = FXCollections.observableArrayList();

    public void initialize(){
        // Listens to listview for a change in selected item
        adoptionListView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newAdoption) -> updateSelectedAdoption(newAdoption)
        );
    }

    public void initService(SessionFactory sessionFactory){
        this.staffService = new StaffService(sessionFactory);
        this.animalService = new AnimalService(sessionFactory);
        this.adoptionService = new AdoptionService(sessionFactory);
        this.userService = new UserService(sessionFactory);
    }

    void setMainScreenController(MainScreenController mainScreenController){
        this.mainScreenController = mainScreenController;
    }

    void setAnimalViewPaneController(AnimalViewPaneController animalViewPaneController){
        this.animalViewPaneController = animalViewPaneController;
    }

    private void updateSelectedAdoption(Adoption adoption){
        this.selectedAdoption = adoption;
        // If an adoption is cancelled, selectedAdoption will be null
        if(selectedAdoption != null){
            // Update elements
            Animal selectedAdoptionAnimal = animalService.getAnimal(adoption.getAdoptionId().getAnimalID());
            // Disable or enable button and checkbox based on adoption
            this.adoptionApprovedCheckBox.setSelected(adoption.isApproved());
            this.cancelAdoptionButton.setDisable(adoption.isApproved());
            // load the animal and update the text with details
            animalViewPaneController.loadAnimalView(new AnimalView(selectedAdoptionAnimal));
            selectedAdoptionUserText.setText(
                    userService.getUser(selectedAdoption.getAdoptionId().getUserID()).getUsername() +
                            " requests "+ selectedAdoptionAnimal.getName());
        }
    }

    // Refreshes the adoption table
    public void updateAdoptions(){
        User user = mainScreenController.getLoggedInUser();
        Staff staff = staffService.getStaff(user.getUserID());
        adoptions = FXCollections.observableArrayList();
        if (staff !=  null){
            this.adoptionApprovedCheckBox.setDisable(!staff.canApproveAdoptions());
            adoptions.addAll(adoptionService.getAllAdoptions());
        } else {
            adoptions.addAll(adoptionService.getAdoptionsByUser(user.getUserID()));
            this.adoptionApprovedCheckBox.setDisable(true);
        }
            adoptionListView.setItems(adoptions);
            adoptionListView.refresh();
    }

    @FXML
    private void cancelAdoption(){
        if (this.selectedAdoption != null){
            Animal animal = animalService.getAnimal(this.selectedAdoption.getAdoptionId().getAnimalID());
            animal.setAdopted(false);
            animalService.update(animal);
            this.adoptionService.delete(this.selectedAdoption);
        }
        updateAdoptions();
    }

    @FXML
    private void approveAdoption(){
        // If an adoption is selected
        if (this.selectedAdoption != null){
            // Approve or un-approve the adoption
            selectedAdoption.setApproved(adoptionApprovedCheckBox.isSelected());
            this.cancelAdoptionButton.setDisable(adoptionApprovedCheckBox.isSelected());
            // If the adoption is approved
            if (selectedAdoption.isApproved()){
                // Remove other adoption requests for this animal, to prevent adoption requests for nonexistent animals
                List<Adoption> animals = adoptionService.getAdoptionsByAnimal(selectedAdoption.getAdoptionId().getAnimalID());
                for(Adoption adoption : animals){
                    if(adoption.getAdoptionId().getUserID() != mainScreenController.getLoggedInUser().getUserID()){
                        adoptionService.delete(adoption);
                        updateAdoptions();
                    }
                }
            }
            // Update the database tables and refresh the views
            adoptionService.update(selectedAdoption);
            Animal animal = animalService.getAnimal(selectedAdoption.getAdoptionId().getAnimalID());
            animal.setAdopted(adoptionApprovedCheckBox.isSelected());
            animalService.update(animal);
            mainScreenController.populateTable();
        }
    }

}
