package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.account.Staff;
import model.account.User;
import service.StaffService;
import service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Controller handling actions relating to the login screen
 * Also handles generation and storing of user password hash strings with salts
 */
public class LoginScreenController {
    @FXML
    private TextField usernameField;
    @FXML
    private PasswordField passwordField;

    private SessionFactory sessionFactory;
    private  UserService userService;
    private StaffService staffService;

    @FXML
    public void loginButtonPushed(ActionEvent actionEvent) {
        //Gets the stage info
        Stage window = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        login(window);
        // TODO Validation & sanitation of inputs
    }

    @FXML
    public void signUpButtonPushed(){
        List<User> users = userService.getAllUsers();
        boolean  firstUser = users.size() == 0;

        // Regular expression pattern by Sidney Ellis
        Pattern regexPattern = Pattern.compile("(?=.*[<>;:*&£^(){}'~#@.><,`¬+=!?$|])(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$");

        // Input validation ensures users choose a secure password
        if(!regexPattern.matcher(passwordField.getText()).matches()){
            generateAlert("Insecure password, please use at least one:\n" +
                    "Special character\n" +
                    "Upper case character\n" +
                    "Lower Case character\n" +
                    "Number"
            );
            return;
        }

        // Ensures user does not already exist
        User foundUser = null;
        for (User user:users){
            if(user.getUsername().toUpperCase().equals(usernameField.getText().toUpperCase())){
                foundUser = user;
            }
        }
        if(foundUser != null){
            generateAlert("User already exists! Try logging in");

        } else {
            String salt = generateSalt();
            String passwordHash = generateHash(passwordField.getText(), salt);
            User newUser = new User(usernameField.getText(), passwordHash, salt);
            this.userService.create(newUser);

            // If this is the first user, approve account and grant full permissions
            if(firstUser){
                this.userService.delete(newUser);

                Staff staff = new Staff(newUser);
                staff.setApproved(true);
                staff.setCanApproveAdoptions(true);
                staff.setCanAddAnimals(true);
                staff.setCanManageUsers(true);
                staff.setCanRemoveAnimals(true);
                this.staffService.create(staff);
                generateAlert("Account created, log in!");
            } else {
                generateAlert("Account created, please wait for staff approval");
            }
        }
    }

    // Handles validation of entered credentials
    private void login(Stage window){
        List<User> users = userService.getAllUsers();
        User foundUser = null;
        for (User user:users){
            if(user.getUsername().toUpperCase().equals(usernameField.getText().toUpperCase())){
                foundUser = user;
            }
        }

        if(foundUser != null){
            // getText() used for security, to minimise storage of user password string
            String passwordHash = generateHash(passwordField.getText(), foundUser.getSalt());
            if (passwordHash.equals(foundUser.getPasswordHash())) {
                if(foundUser.isApproved()) {
                    // Credentials are correct, so launch the main screen
                    launchMainScreen(foundUser, window);
                } else {
                    // User account has not been approved for login yet
                    generateAlert("Your account must be approved by a staff member before you can login");
                }
            } else {
                // Password is incorrect, prompt for try again
                generateAlert("Password incorrect, try again");
            }
        } else {
            // User is not found in the system, prompt for account creation
            generateAlert("User not found, try signing up");
        }
    }

    // Generates a sha-256 hash of input
    private String generateHash(String password, String salt){
        String sha256hex = DigestUtils.sha256Hex(password + salt);
        return sha256hex;
    }

    // Generates a new random 10 letter string
    public String generateSalt(){
        Random random = new Random();
        StringBuilder salt = new StringBuilder();
        String alphabet =  "abxdefghijklmnopqrstuvwxyz";
        for(int i = 0; i < 10; i++) {
            salt.append(alphabet.charAt(random.nextInt(25)));
        }
        return salt.toString();
    }

    // Useful method for generating popups with custom text
    public void generateAlert(String message){
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Alert");
        alert.setContentText(message);
        alert.show();
    }

    // Launches the main screen
    private void launchMainScreen(User loggedInUser, Stage window){
        try {
            //Creates a new FXMLLoader object and loads in the main controller
            FXMLLoader loader = new FXMLLoader();
            URL url = getClass().getResource("/mainScreen.fxml").toURI().toURL();
            loader.setLocation(url);
            Parent mainScreenParent = loader.load();

            //Sets up the scene with the elements from the FXML file
            Scene mainScreenScene = new Scene(mainScreenParent);
            mainScreenScene.getStylesheets().add("/style.css");

            //Initialise the main screen with the signed in user
            MainScreenController controller = loader.getController();
            controller.setLoggedInUser(loggedInUser);
            controller.setSessionFactory(this.sessionFactory);

            //Assigns details to the new scene (Icon, title) then shows the window.
            window.setScene(mainScreenScene);
            window.setTitle("Sheltered");
            window.setFullScreen(true);
            window.show();

        } catch (IOException | URISyntaxException e) {
            System.out.println("Error: " + e.getMessage() + e.getCause());
            e.printStackTrace();

        }
    }

    // Sets up database connection services
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
        this.userService = new UserService(sessionFactory);
        this.staffService = new StaffService(sessionFactory);
    }
}
