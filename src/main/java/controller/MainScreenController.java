package controller;

import com.jfoenix.controls.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import model.*;
import model.account.Staff;
import model.account.User;
import model.animal.Animal;
import service.animal.AnimalService;
import service.StaffService;
import org.hibernate.SessionFactory;

import java.util.ArrayList;

/**
 * This class handles the interactions with the main TableView section
 * It also acts as a central base controller that links the others.
 * TabPanes are attached here
 */
public class MainScreenController {
    @FXML
    private BorderPane borderPane;
    @FXML
    private TableView<AnimalView> animalTableView;
    @FXML
    private JFXTabPane tabPane;
    @FXML
    private Tab addAnimalTab;
    @FXML
    private Tab manageTab;
    @FXML
    private TableColumn<AnimalView, SimpleStringProperty> nameColumn;
    @FXML
    private TableColumn<AnimalView, SimpleStringProperty> animalColumn;
    @FXML
    private TableColumn<AnimalView, SimpleStringProperty> descriptionColumn;
    @FXML
    private TableColumn<AnimalView, SimpleStringProperty> ageColumn;
    @FXML
    private TableColumn<AnimalView, SimpleStringProperty> costColumn;
    @FXML
    private SearchPaneController searchPaneController;
    @FXML
    private AddAnimalPaneController addAnimalPaneController;
    @FXML
    private ManagePaneController managePaneController;
    @FXML
    private AccountPaneController accountPaneController;
    @FXML
    private AdoptionPaneController adoptionPaneController;
    @FXML
    private AnimalViewPaneController animalViewPaneController;

    private AnimalService animalService;
    private ObservableList<AnimalView> animals = FXCollections.observableArrayList();
    private ObservableList<User> users = FXCollections.observableArrayList();
    private ObservableList<Adoption> adoptions = FXCollections.observableArrayList();
    private User loggedInUser;
    private SessionFactory sessionFactory;

    private ArrayList<JFXCheckBox> permissionCheckboxes = new ArrayList<>();
    private StaffService staffService;

    public void initialize() {
        // Setup table columns
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        ageColumn.setCellValueFactory(new PropertyValueFactory<>("age"));
        animalColumn.setCellValueFactory(new PropertyValueFactory<>("animalGroup"));
        descriptionColumn.setCellValueFactory(new PropertyValueFactory<>("description"));
        costColumn.setCellValueFactory(new PropertyValueFactory<>("cost"));

        // Add listener for clicks on table rows to select and display animal
        animalTableView.setRowFactory(tv -> {
            TableRow<AnimalView> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty()) {
                    AnimalView rowData = row.getItem();
                    animalViewPaneController.setSelectedRow(rowData);
                    animalViewPaneController.loadAnimalView(rowData);
                }
            });
            return row;
        });

    }

    void setLoggedInUser(User user) {
        this.loggedInUser = user;
    }

    // Populates the main animal TableView
    void populateTable() {
        // Empty the table and refresh
        this.animals.removeAll();
        this.animalTableView.getItems().removeAll();
        this.animalTableView.refresh();

        // Sets up a new list and populates it
        this.animals = FXCollections.observableArrayList();
        for (Animal animal : this.animalService.getAllAnimals()) {
            if(!animal.isAdopted()){
                AnimalView newAnimal = new AnimalView(animal);
                this.animals.add(newAnimal);
            }
        }
        this.animalTableView.setItems(this.animals);
        this.animalTableView.refresh();
    }

    void updateAnimalTable(ObservableList<AnimalView> animalViews){
        this.animalTableView.setItems(null);
        this.animalTableView.refresh();
        this.animalTableView.setItems(animalViews);
        this.animalTableView.refresh();
    }

    void generateError(String message, String stackTrace) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle("Error has occurred");
        alert.setHeaderText(message);
        alert.setContentText(stackTrace);
        alert.show();
    }

    User getLoggedInUser(){
        return this.loggedInUser;
    }

    public void setThemeColour(String color){
        borderPane.setStyle(color);
    }

    @FXML
    void setAnimalVisible(boolean visible) {
        animalColumn.setVisible(visible);
    }

    @FXML
    void setNameVisible(boolean visible) {
        nameColumn.setVisible(visible);
    }

    @FXML
    void setCostVisible(boolean visible) {costColumn.setVisible(visible);}

    @FXML
    void setAgeVisible(boolean visible) {
        ageColumn.setVisible(visible);
    }

    @FXML
    void setDescriptionVisible(boolean visible) {
        descriptionColumn.setVisible(visible);
    }

    @FXML
    private void aboutPage() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("About Sheltered");
        alert.setHeaderText("Sheltered 0.1.0");
        alert.setContentText("Sheltered is a management system for animal shelters\n" +
                "The leftmost menu is for navigation between key functions\n" +
                "The centre table displays all animals applicable to current filter or search query \n" +
                "The rightmost pane shows information about the selected animal. \n" +
                "\nFor More information, contact: james.mcnicholas@cgi.com");
        alert.show();
    }
    // Initialises services and other items that require services first
    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;

        // Initialise database services
        this.animalService = new AnimalService(this.sessionFactory);
        this.staffService = new StaffService(this.sessionFactory);

        searchPaneController.setMainScreenController(this);
        searchPaneController.initService(this.sessionFactory);

        addAnimalPaneController.setMainScreenController(this);
        addAnimalPaneController.initService(this.sessionFactory);

        managePaneController.setMainScreenController(this);
        managePaneController.initService(this.sessionFactory);

        accountPaneController.setMainScreenController(this);
        accountPaneController.initService(this.sessionFactory);

        animalViewPaneController.initService(this.sessionFactory);
        animalViewPaneController.setMainScreenController(this);
        animalViewPaneController.setAdoptionPaneController(adoptionPaneController);

        adoptionPaneController.setMainScreenController(this);
        adoptionPaneController.initService(this.sessionFactory);
        adoptionPaneController.setAnimalViewPaneController(animalViewPaneController);

        populateTable();
        adoptionPaneController.updateAdoptions();

        // Handles permissions applicable to main screen elements
        Staff staffUser = staffService.getStaff(this.loggedInUser.getUserID());
        if(staffUser != null){
            this.addAnimalTab.setDisable(!staffUser.canAddAnimals());
            this.manageTab.setDisable(!staffUser.canManageUsers());

        } else {
            this.addAnimalTab.setDisable(true);
            this.manageTab.setDisable(true);
        }
    }
}

